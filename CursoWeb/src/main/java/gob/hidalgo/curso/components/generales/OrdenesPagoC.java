package gob.hidalgo.curso.components.generales;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gob.hidalgo.curso.components.MensajesC;
import gob.hidalgo.curso.database.generales.ClienteOE;
import gob.hidalgo.curso.database.generales.OrdenPagoEO;
import gob.hidalgo.curso.utils.Modelo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component ("OrdenesPagoC")
public class OrdenesPagoC {
	
	@Autowired
	private SqlSession sqlSession;
	
	@Autowired
	private MensajesC mensajesC;

	public OrdenesPagoC() {
		super();
		log.debug("Se crea componente OrdenesPagoC");
	}
	
	public Modelo<OrdenPagoEO> modelo(ClienteOE cliente){
		return new Modelo<OrdenPagoEO>(cliente.getOrdenesPago());
	}
	
	public OrdenPagoEO nuevo () {
		return new OrdenPagoEO();
	}
	
	public boolean guardar(ClienteOE cliente, OrdenPagoEO orden) {
		HashMap<String, Object> mapaParametros;
		if(orden.getId()==null) {
			mapaParametros = new HashMap<String, Object>();
			mapaParametros.put("cliente", cliente);
			mapaParametros.put("ordenPago", orden);
			sqlSession.insert("generales.ordenesPago.insertar",mapaParametros);
			mensajesC.mensajeInfo("Orden de Pago agregada exitosamente");
		}else {
			sqlSession.update("generales.ordenesPago.actualizar",orden);
			mensajesC.mensajeInfo("Orden de Pago actualizada exitosamente");
		}
		return true;
	}

}
